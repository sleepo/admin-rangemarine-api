<?php

return [
    'debug' => true,
    'vendor' => env('ADMIN_RANGEMARINE_API_VENDOR'),

    'http' => [
        'base_uri' => env('ADMIN_RANGEMARINE_API_HTTP_BASE_URI', 'https://admin.rangemarine.ru/wp-json/rm/v1/'),
        'timeout' => 10,
        'allow_redirects' => false
    ]
];