<?php namespace Brainylab\AdminRangeMarine\Api;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider {

    public function boot()
    {
        $configPath = __DIR__ . '/config.php';

        $this->publishes([
            $configPath => config_path('admin-rangemarine-api.php'),
        ], 'config');

        $this->mergeConfigFrom( $configPath, 'admin-rangemarine-api' );

        $this->loadViewsFrom(__DIR__.'/../views', 'admin-rangemarine-api');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton( Client::class, function($app)
        {
            $apiClient = new Client([
                'vendor' => config('admin-rangemarine-api.vendor'),
                'debug' => config('admin-rangemarine-api.debug')
            ]);

            $config = config('admin-rangemarine-api.http');

            $apiClient->setHttpClient(new \GuzzleHttp\Client($config));

            return $apiClient;
        } );
    }

}