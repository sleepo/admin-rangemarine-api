<?php namespace Brainylab\AdminRangeMarine\Api;

use GuzzleHttp\Client as HttpClient;

class Client {

    /**
     * @var HttpClient|null
     */
    protected $httpClient;

    protected $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function setHttpClient(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getHttpClient()
    {
        return $this->httpClient;
    }

    public function getBrokerageList(array $params = [])
    {
        $response = $this->httpClient->get('brokerage', [
            'query' => [
                'vendor' => array_get( $this->params, 'vendor' ),
            ]
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);

        return new Response($result);
    }

    public function getBrokerageBoat(array $params = [])
    {
        $response = $this->httpClient->get( implode('/', [ 'brokerage', array_get($params, 'id') ]), [
            'query' => [
                'vendor' => array_get( $this->params, 'vendor' ),
            ]
        ] );

        $result = \GuzzleHttp\json_decode( $response->getBody(), true );

        return new Response($result);
    }

    public function getNotifyUrl( array $params = [] )
    {
        return config('admin-rangemarine-api.http.base_uri') . 'brokerage/' . array_get($params, 'id') . '/notify';
    }

    public function getParams()
    {
        return $this->params;
    }

}
