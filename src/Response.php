<?php namespace Brainylab\AdminRangeMarine\Api;

class Response {

    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function data( $key = null)
    {
        $c = ['data'];
        if ($key)
        {
            $c[] = $key;
        }
        $c = implode('.', $c);

        return array_get($this->data, $c);
    }

    public function extra( $key )
    {
        return array_get($this->data, $key);
    }

}