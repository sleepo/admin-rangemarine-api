<?php namespace Brainylab\AdminRangeMarine\Api;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Response;

class HtmlPresenter {

    protected $apiClient;

    public function __construct(Client $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    protected function handleHttpError(callable $callback)
    {
        try
        {
            return call_user_func($callback);
        }
        catch (ClientException $exception)
        {
            if ( $exception->getCode() === 404 )
            {
                return new Response( view('admin-rangemarine-api::errors.404'), 404 );
            }
            if ( config('admin-rangemarine-api.debug') )
            {
                throw $exception;
            }
            else
            {
                return new Response( view('admin-rangemarine-api::errors.503'), 503 );
            }
        }
        catch (\Exception $exception)
        {
            if ( config('admin-rangemarine-api.debug') )
            {
                throw $exception;
            }
            else
            {
                return new Response( view('admin-rangemarine-api::errors.503'), 503 );
            }
        }
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getBrokerageList(array $params = [])
    {
        return $this->handleHttpError(function() use ($params)
        {
            $result = $this->apiClient->getBrokerageList($params);

            return [new Response( view('admin-rangemarine-api::list', [
                'boats' => $result->data('boat') // todo rename to boats
            ]), 200), $result->data('vendor.meta') ];
        });
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getBrokerageBoat(array $params = [])
    {
        return $this->handleHttpError(function() use ($params)
        {
            $result = $this->apiClient->getBrokerageBoat($params);

            $boat = $result->data();

            $photos = collect( [ 'in_motion', 'interior', 'exterior' ] )->map(function($key) use ($boat)
            {
                $photo = array_get( $boat, 'photo.'.$key );
                if ( !$photo )
                {
                    $photo = array_get( $boat, 'boat.photo.'.$key );
                }
                return $photo;
            })->flatten(1);

            $vendor = array_get($this->apiClient->getParams(), 'vendor');

            return [new Response( view('admin-rangemarine-api::single', [
                'boat' => $boat,
                'photos' => $photos,
                'logo_url' => array_get($params, 'logo_url'),
                'vendor' => $vendor,
                'notify_url' => $this->apiClient->getNotifyUrl( [ 'id' => array_get($boat, 'id') ] ),
                'current_url' => \Request::url()
            ]), 200 ), $result->data('meta')];
        });
    }

}