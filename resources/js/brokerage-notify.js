var $ = require('jquery');

$(function(){

    $('[data-brokerage-notify]').each(function(){
        var $el = $(this);

        $el.find('[data-brokerage-notify-content]').hide();

        var $open = $el.find('[data-brokerage-notify-open]');
        var $content = $el.find('[data-brokerage-notify-content]');

        $open.fancybox({
            autoSize: false,
            maxWidth: 300,
            maxHeight: 450,
            afterLoad: function(current, previous){

                var $content = $(current.content),
                    $error = $content.find('[data-brokerage-notify-error]');

                var showPanel = function(panel){
                    $content.find('[data-brokerage-notify-panel]').hide();
                    $content.find('[data-brokerage-notify-panel="'+panel+'"]').show();
                };

                $content.on('submit', '[data-brokerage-notify-panel="form"] form', function(e){
                    e.preventDefault();

                    var $el = $(this);
                    $error.empty();

                    var data = {
                        name: $el.find('[name="name"]').val(),
                        email: $el.find('[name="email"]').val(),
                        phone: $el.find('[name="phone"]').val(),
                        message: $el.find('[name="message"]').val(),
                        url: $el.find('[name="url"]').val(),
                        vendor: $el.find('[name="vendor"]').val()
                    };

                    var url = $el.prop('action');

                    $.post( url, data, function(){
                        showPanel('complete');
                    }).fail(function(){
                        $error.html('<div class="alert alert-error">Произошла ошибка, попробуйте еще раз.</div>')
                        showPanel('form');
                    });

                    showPanel('process');

                });

                $content.on('click', '[data-brokerage-notify-close]', function(e){
                    e.preventDefault();
                    $.fancybox.close( true );
                });

                showPanel('form');

            }

        });

    });

});