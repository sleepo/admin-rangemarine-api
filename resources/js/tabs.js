var $ = require('jquery');

$(function(){
    $('[data-ar-tabs]').each(function(index, el){

        console.log('init tabs');

        var $el = $(el);

        var $active = $el.find('[data-ar-tabs-nav].active');
        
        if ( !$active.length ){
            $active.find('[data-ar-tabs-nav]:first');
        }
        
        var prevActive = null;
        
        var showTab = function(sel){

            console.log('showTab', sel);

            if (prevActive){
                hideTab(prevActive)    
            }
            
            $el.find("[data-ar-tabs-nav='" + sel + "']").addClass('active');
            $el.find( sel ).addClass('active');
        
            prevActive = sel;
        };
        
        var hideTab = function(sel){
            console.log('hide tab', sel);
            $el.find("[data-ar-tabs-nav='" + sel + "']").removeClass('active');
            $el.find( sel ).removeClass('active');
        };

        showTab($active.data('ar-tabs-nav'));
        
        $el.on('click', '[data-ar-tabs-nav]', function(e){
            
            e.preventDefault();
            var $el = $(this);
            console.log('click', $el);
            showTab($el.data('ar-tabs-nav'));
        });

    });
});